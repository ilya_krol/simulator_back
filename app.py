from flask import Flask, render_template, request, Response, jsonify, send_from_directory
from control import *
from ast import literal_eval as make_tuple
from math import *

import matplotlib

matplotlib.use("Agg")
import matplotlib.pyplot as plt
import uuid
import numpy as np

app = Flask(__name__)


@app.route('/', methods=['GET'])
def index():
    return 'Hello, world!'


@app.route('/img/<path:filename>')
def download_file(filename):
    # print(filename)
    return send_from_directory('img', filename)


@app.route('/step-response', methods=['OPTIONS'])
@app.route('/impulse-response', methods=['OPTIONS'])
@app.route('/forced-response', methods=['OPTIONS'])
@app.route('/bode-plot', methods=['OPTIONS'])
@app.route('/nyquist-plot', methods=['OPTIONS'])
def stepResponseOptions():
    response = Response()
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Methods'] = 'POST'
    response.headers['Access-Control-Allow-Headers'] = 'content-type'
    return response


def extractGraph(items, links):
    prepareItems(items)
    items = {item['id']: item for item in items}
    links = [(link['start'], link['end'], link['positive']) for link in links]
    return extractGraph_(items, links)


def prepareItems(items):
    for item in items:
        prepareItem(item)


def prepareItem(item):
    if (item['type'] == 'element'):
        item['tf'] = make_tuple(item['tf'])
    if (item['type'] == 'input-signal'):
        item['input'] = make_tuple(item['input'])


def extractGraph_(items, links):
    result = {}

    def addSrcLink(id, outputLink):
        node = result.get(id)
        if (not node):
            result[id] = (items[id], [], [outputLink], [])
        else:
            _, _, outputLinks, _ = node
            outputLinks.append(outputLink)

    def addDstLink(id, inputLink, positive):
        node = result.get(id)
        if (not node):
            result[id] = (items[id], [inputLink], [], [positive])
        else:
            _, inputLinks, _, inputPositives = node
            inputLinks.append(inputLink)
            inputPositives.append(positive)

    for link in links:
        addSrcLink(link[0], link[1])
        addDstLink(link[1], link[0], link[2])

    # print('graph', result)
    return result


def getTransferFunction(graph):
    def graphToSystemElements(graph):
        # print('-----')
        result = {}
        i = 0
        o = 0
        inputInput = []
        orderedInputs = []
        outputOutput = []

        for key, value in graph.items():
            item, inputLinks, outputLinks, inputPositives = value

            # print('key', key)
            # print('inputLinks', inputLinks)
            # print('inputPositives', inputPositives)
            inputs = []
            for _ in inputLinks:
                i = i + 1
                inputs.append(i)


            # print('outputLinks', outputLinks)
            outputs = []
            # for _ in outputLinks:
            o = o + 1
            outputs.append(o)

            if (item['type'] == 'element'):
                sys = tf2ss(getItemTf(item))
            elif (item['type'] == 'input-signal'):
                sys = ss([], [], [], 1.)
                i = i + 1
                inputs.append(i)
                inputInput.append(i)
                orderedInputs.append(item['id'])

            elif (item['type'] == 'output-signal'):
                sys = ss([], [], [], 1.)
                outputOutput.append(o)

            elif (item['type'] == 'summator'):
                D = []
                # print('D', D)
                for positive in inputPositives:
                    if (positive == 'true'):
                        D.append(1.)
                    else:
                        D.append(-1.)
                    # print('D', D)
                sys = ss([], [], [], D)
                # print(sys)

            # print('inputs', inputs)
            # print('outputs', outputs)
            # print(ss2tf(sys))
            # print(sys)

            sysElement = (sys, inputs, outputs)
            result[key] = sysElement
            # print('-----')


        # print('inputInput', inputInput)
        # print('outputOutput', outputOutput)
        # print('orderedInputs', orderedInputs)
        # print('-----')

        return result, inputInput, outputOutput, orderedInputs

    def getElementLinks(graph, systemElements):
        systems = []
        links = []
        # print('--**--')

        for key, (item, _, outputLinks, _) in graph.items():
            sys, _, outputs = systemElements[key]

            systems.append(sys)

            for outputLink in outputLinks:
                oo = outputs[0]

                _, inputLinks,_,_ = graph[outputLink]
                _, inputs, _ = systemElements[outputLink]
                ii = inputs[inputLinks.index(key)]

                # print('oo', oo, 'ii', ii)
                links.append([ii, oo])

                # print('--**--')

        # print(links)
        return systems, links

    systemElements, inIn, outOut, orderedInputs =  graphToSystemElements(graph)
    systems, elementLinks = getElementLinks(graph, systemElements)

    appended = append(*systems)
    # print('appended', appended)
    # print('elementLinks', elementLinks)
    # print('inin', inIn)
    # print('outout', outOut)
    connected = connect(appended, elementLinks, inIn, outOut)
    # print('connected', connected)
    result = ss2tf(connected)
    print(result)

    return result, orderedInputs


def getItemTf(item):
    num, den = item['tf']
    return tf(num, den)


def plotChart(x, y):
    plt.plot(x, y)
    plt.grid(True)
    filename = str(uuid.uuid4()) + '.png'
    plt.savefig('img/' + filename)
    plt.close()
    return filename

def plotCharts(x, Y):
    for y in Y:
        plt.plot(x, y)
    plt.grid(True)
    filename = str(uuid.uuid4()) + '.png'
    plt.savefig('img/' + filename)
    plt.close()
    return filename

def plotBodeChard():
    filename = str(uuid.uuid4()) + '.png'
    plt.savefig('img/' + filename)
    plt.close()
    return filename


def getTimeVec(T):
    N = 1000
    delta = T/N
    times = np.empty(N + 1)
    for i in range(0, N + 1):
        times[i] = i * delta
    return times

@app.route('/step-response', methods=['POST'])
def stepResponse():
    systemTf, _ = getSystemTf(request.get_json())
    x, y = step_response(systemTf, getTimeVec(int(request.get_json()['time'])))
    filename = plotChart(x, y)
    return makeResponse(filename)


@app.route('/impulse-response', methods=['POST'])
def impulseResponse():
    systemTf, _ = getSystemTf(request.get_json())
    x, y = impulse_response(systemTf, getTimeVec(int(request.get_json()['time'])))
    filename = plotChart(x, y)
    return makeResponse(filename)

@app.route('/forced-response', methods=['POST'])
def forcedResponse():
    systemTf, inputElements = getSystemTf(request.get_json())
    time = int(request.get_json()['time'])
    timeVec = getTimeVec(time)
    inputsVec = getInputsVec(inputElements, timeVec)
    x, Y, _ = forced_response(systemTf, timeVec, inputsVec)
    lY = len(Y.shape)
    if lY == 1:
        filename = plotChart(x, Y)
    elif lY == 2:
        filename = plotCharts(x, Y)
    return makeResponse(filename)

@app.route('/bode-plot', methods=['POST'])
def bodePlot():
    systemTf, _ = getSystemTf(request.get_json())
    bode_plot(systemTf)
    filename = plotBodeChard()
    return makeResponse(filename)

@app.route('/nyquist-plot', methods=['POST'])
def nyquistPlot():
    systemTf, _ = getSystemTf(request.get_json())
    nyquist_plot(systemTf)
    filename = plotBodeChard()
    return makeResponse(filename)


def makeResponse(filename):
    response = jsonify(filename)
    response.headers['Access-Control-Allow-Origin'] = '*'
    return response

def getInputsVec(inputElements, times):
    inputs = []
    for inputElement in inputElements:
        inputs.append(getInput(inputElement, times))
    return inputs

def getInput(inputElement, times):
    if inputElement['inputType']=='step':
        return getStepInput(times, inputElement['input'])
    elif inputElement['inputType']=='rectangular':
        return getRectangularInput(times, inputElement['input'])
    elif inputElement['inputType']=='linear':
        return getLinearInput(times, inputElement['input'])
    elif inputElement['inputType']=='harmonic':
        return getHarmonicInput(times, inputElement['input'])

def getStepInput(times, input):
    k, t = input
    N = 1000

    inputSignal = np.empty(N + 1)
    i = 0
    for time in times:
        if time < t:
            inputSignal[i] = 0
        else:
            inputSignal[i] = k
        i = i + 1
    return inputSignal

def getRectangularInput(times, input):
    k, t1, t2 = input
    N = 1000

    inputSignal = np.empty(N + 1)
    i = 0
    for time in times:
        if time < t1:
            inputSignal[i] = 0
        elif time <= t2:
            inputSignal[i] = k
        else:
            inputSignal[i] = 0
        i = i + 1

    return inputSignal


def getLinearInput(times, input):
    k, t1 = input
    N = 1000

    inputSignal = np.empty(N + 1)
    i = 0
    for time in times:
        if time < t1:
            inputSignal[i] = 0
        else:
            inputSignal[i] = k * (time - t1)
        i = i + 1

    return inputSignal

def getHarmonicInput(times, input):
    k, v, t1 = input
    N = 1000

    inputSignal = np.empty(N + 1)
    i = 0
    for time in times:
        if time < t1:
            inputSignal[i] = 0
        else:
            inputSignal[i] =  k * sin((time - t1)*2*pi*v)
        i = i + 1

    return inputSignal


def getSystemTf(rq):
    graph = extractGraph(rq['items'], rq['links'])
    sysTf, orderedInputs = getTransferFunction(graph)

    inputElements = []
    for inputId in orderedInputs:
        inputElements.append(graph[inputId][0])

    return sysTf, inputElements


if __name__ == '__main__':
    app.run()
